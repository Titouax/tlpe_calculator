lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "tlpe_calculator/version"

Gem::Specification.new do |spec|
  spec.name          = "tlpe_calculator"
  spec.version       = TlpeCalculator::VERSION
  spec.authors       = ["Titouan Dessus"]
  spec.email         = ["titouan.dessus@gmail.com"]

  spec.summary       = %q{A service that calculate the french tax on local advertisment}
  spec.description   = %q{TLPE Calculator is a ready to go service that help french administrations and companies to calculate there tax on local advertisment.}
  spec.homepage      = "https://gitlab.com/Titouax/tlpe_calculator"
  spec.license       = "MIT"

  spec.metadata["allowed_push_host"] = "https://gitlab.com/Titouax/tlpe_calculator'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/Titouax/tlpe_calculator"
  spec.metadata["changelog_uri"] = "https://gitlab.com/Titouax/tlpe_calculator/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
